module gitlab.com/blauwe-knop/vorderingenoverzicht/demo-simulation-process

go 1.22.3

require (
	github.com/jessevdk/go-flags v1.6.1
	go.uber.org/zap v1.27.0
)

require (
	go.uber.org/multierr v1.11.0 // indirect
	golang.org/x/sys v0.29.0 // indirect
)
