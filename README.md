# demo-simulation-process

Procescomponent of demo simulation in the 'Vorderingenoverzicht' system.

## Installation

Prerequisites:

- [Git](https://git-scm.com/)
- [Golang](https://golang.org/doc/install)

1. Download the required Go dependencies:

```sh
go mod download
```

1. Now start the demo simulation process:

```sh
go run cmd/demo-simulation-process/*.go --listen-address 0.0.0.0:8080
```

By default, the application will run on port `80`, so specifically set port 8080 to run it locally.

## Deployment
