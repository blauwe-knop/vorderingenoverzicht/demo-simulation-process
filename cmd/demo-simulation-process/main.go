// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package main

import (
	"errors"
	"fmt"
	"log"
	"net/http"

	"go.uber.org/zap"

	"gitlab.com/blauwe-knop/vorderingenoverzicht/demo-simulation-process/internal/http_infra"
)

type options struct {
	ListenAddress           string `long:"listen-address" env:"LISTEN_ADDRESS" default:"0.0.0.0:80" description:"Address for the demo-simulation-process api to listen on. Read https://golang.org/pkg/net/#Dial for possible tcp address specs."`
	DemoSourceSystemAddress string `long:"demo-source-system-address" env:"DEMO_SOURCE_SYSTEM_ADDRESS" default:"http://localhost:80" description:"Demo source system address."`
	LogOptions
}

func main() {
	cliOptions, err := parseOptions()
	if err != nil {
		log.Fatalf("failed to parse options: %v", err)
	}

	logger, err := newLogger(cliOptions.LogOptions)
	if err != nil {
		log.Fatalf("failed to create logger: %v", err)
	}

	paths := []string{
		"/v4/financial_claims_information",
		"/v4/add_event",
		"/v4/add_achterstand",
		"/v4/add_contact_opties",
		"/v4/reset",
		"/v4/openapi.json",
		"/v4/openapi.yaml",
		"/v4/health",
		"/v4/health/check",
	}

	for _, path := range paths {
		proxy, secondaryErr := http_infra.NewProxy(cliOptions.DemoSourceSystemAddress, logger)
		if secondaryErr != nil {
			logger.Fatal("failed to initialize proxy", zap.Error(secondaryErr))
			panic(secondaryErr)
		}

		http.HandleFunc(path, http_infra.ProxyRequestHandler(proxy))
	}

	logger.Info(fmt.Sprintf("start listening on %s", cliOptions.ListenAddress))
	err = http.ListenAndServe(cliOptions.ListenAddress, nil)
	if err != nil {
		if !errors.Is(err, http.ErrServerClosed) {
			panic(err)
		}
	}
}

func newLogger(logOptions LogOptions) (*zap.Logger, error) {
	config := logOptions.ZapConfig()
	logger, err := config.Build()
	if err != nil {
		return nil, fmt.Errorf("failed to create new zap logger: %v", err)
	}

	return logger, nil
}
