FROM golang:1.23.1-alpine AS build

RUN apk add --update --no-cache git

COPY ./cmd /go/src/demo-simulation-process/cmd
COPY ./internal /go/src/demo-simulation-process/internal
COPY ./go.mod /go/src/demo-simulation-process/
COPY ./go.sum /go/src/demo-simulation-process/
WORKDIR /go/src/demo-simulation-process
RUN go mod download \
 && go build -o dist/bin/demo-simulation-process ./cmd/demo-simulation-process

# Release binary on latest alpine image.
FROM alpine:3.20

ARG USER_ID=10001
ARG GROUP_ID=10001

COPY --from=build /go/src/demo-simulation-process/dist/bin/demo-simulation-process /usr/local/bin/demo-simulation-process

# Add non-priveleged user. Disabled for openshift
RUN addgroup -g "$GROUP_ID" appgroup \
 && adduser -D -u "$USER_ID" -G appgroup appuser
USER appuser
HEALTHCHECK none
CMD ["/usr/local/bin/demo-simulation-process"]
