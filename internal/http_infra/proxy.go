package http_infra

import (
	"net/http"
	"net/http/httputil"
	"net/url"
	"strings"

	"go.uber.org/zap"
)

var logger *zap.Logger

func NewProxy(targetHost string, zapLogger *zap.Logger) (*httputil.ReverseProxy, error) {
	url, err := url.Parse(targetHost)
	if err != nil {
		return nil, err
	}

	logger = zapLogger.With(zap.String("target_host", targetHost))

	proxy := httputil.NewSingleHostReverseProxy(url)

	originalDirector := proxy.Director
	proxy.Director = func(req *http.Request) {
		originalDirector(req)
		modifyRequest(req)
	}

	// proxy.ModifyResponse = modifyResponse()
	proxy.ErrorHandler = errorHandler()
	return proxy, nil
}

func modifyRequest(req *http.Request) {
	req.Header.Set("X-Proxy", "Simple-Reverse-Proxy")
}

func errorHandler() func(http.ResponseWriter, *http.Request, error) {
	return func(responseWriter http.ResponseWriter, req *http.Request, err error) {
		logger.Error("Got error while modifying response", zap.Error(err))
	}
}

// func modifyResponse() func(*http.Response) error {
// 	return func(resp *http.Response) error {
// 		return errors.New("response body is invalid")
// 	}
// }

// ProxyRequestHandler handles the http request using proxy
func ProxyRequestHandler(proxy *httputil.ReverseProxy) func(http.ResponseWriter, *http.Request) {
	return func(responseWriter http.ResponseWriter, request *http.Request) {
		if !strings.Contains(request.URL.String(), "/health") {
			logger.Info("Proxying request", zap.String("requestUrl", request.URL.String()))
		}

		proxy.ServeHTTP(responseWriter, request)
	}
}
